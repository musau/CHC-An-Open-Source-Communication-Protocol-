#ifndef CHC_H_INCLUDED
#define CHC_H_INCLUDED

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "standart.h"
#include "chcDefines.h"


const int numChars = 256; // Buffer size 
int counter_serial = 0; //   Serial counter 

BOOL newData = true; 

char* CHC_CIHAZ_PROFIL_OLUSTURUCU(char* cihaz_adi, char* yazilim_v, char* donanim_v) {
	char profil[25];
	sprintf(profil, "%s/%s/%s", cihaz_adi, yazilim_v, donanim_v);
	DATA.data_type = 0;
	return profil;
}

char* CHC_HEX_MESAJ_OLUSTURUCU(char* mesaj) {

	char* veriler = (char*)malloc(sizeof(mesaj)* 2);

	int i = 0;
	for (i = 0; i< strlen(mesaj); i++) {

		sprintf(&veriler[i * 2], "%x", mesaj[i]);
	}
	return veriler;
}

int  CHC_CRC_KONTROL(char* message) {

	int  k = 0;
	int sayac = 0;
	char m1[2];
	int i = 0;
	for (i; i < strlen(message); i++) {

		sayac += (int)message[i];
	}

	for (k = 0; k < strlen(message); k += 2)
	{
		strncpy(m1, message, sizeof(int)* 2);
		sayac += (int)m1;
	}

	sayac = sayac % 17;

	return sayac;
}

char* CHC_MESAJ_HAZIRLA(char* message) {

	char* new_message = (char*)malloc(sizeof(int)* 4 + 7);
	DATA.data_startLine = ':';
	DATA.data_length = strlen(message);

	char* data = CHC_HEX_MESAJ_OLUSTURUCU(message);
	memset(DATA.data_data, 0, strlen(DATA.data_data));
	strcat(DATA.data_data, data);

	DATA.data_crc = CHC_CRC_KONTROL(DATA.data_data);

	sprintf(new_message, "%c%02d%02d%s%02d", DATA.data_startLine, DATA.data_length, DATA.data_type, DATA.data_data, DATA.data_crc);
	return new_message;
}

int CHC_READ(char income){

	if (newData == true){

		if (income == START_MARKER){
			newData = false;
			return START_VAL;
		}

		else{
			return ERROR_VAL;
		}

	}
	else{
		
		if (income == END_MARKER) {
			DATA.data_data[counter_serial] = '\0'; // terminate the strin
			counter_serial = 0;
			newData = true;
			return END_VAL;
		}
		else if (income != END_MARKER && newData == false) {
			DATA.data_data[counter_serial] = income;
			counter_serial++;
			return INCOME_VAL;
		}
	}
}


#endif // CHC_TYPE_H_INCLUDED
