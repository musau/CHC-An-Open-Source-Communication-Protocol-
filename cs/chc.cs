﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projeyenideneme2018
{
    class CHC
    {

        public static class MESAJ_CHC
        {
            public static int MESAJ_UZUNLUK;
            public static int MESAJ_TYPE;
            public static string MESAJ_MESAJ;
            public static int MESAJ_CRC;


        }
        public static class CIHAZ
        {
            public static string cihaz_adi;
            public static string cihaz_yazilim_v;
            public static string cihaz_donanim_v;
        }


        char TEST_MODU = 'x';
        char UYGULAMA_MODU = 'y';
        char CIHAZ_TANINDI = 'a';



        public enum CHC_METOD_LIST
        {
            CIHAZ_BILGI = 0
        }



        public static int CHC_COZUCU(string chc_mesaj)
        {
            try
            {
                if (chc_mesaj[0] == ':')
                {

                    MESAJ_CHC.MESAJ_MESAJ = "";
                    MESAJ_CHC.MESAJ_TYPE = 0;
                    MESAJ_CHC.MESAJ_UZUNLUK = 0;
                    MESAJ_CHC.MESAJ_CRC = 0;

                    string buffer_mesaj = "";

                    chc_mesaj = chc_mesaj.Substring(1, chc_mesaj.Length - 1);
                    MESAJ_CHC.MESAJ_UZUNLUK = Convert.ToInt32(chc_mesaj.Substring(0, 2));
                    MESAJ_CHC.MESAJ_TYPE = Convert.ToInt32(chc_mesaj.Substring(2, 2));


                    for (int i = 4; i < (MESAJ_CHC.MESAJ_UZUNLUK * 2) + 4; i++)
                    {

                        buffer_mesaj += chc_mesaj[i];

                    }

                    for (int i = 0; i < MESAJ_CHC.MESAJ_UZUNLUK * 2; i += 2)
                    {
                        string buf = buffer_mesaj.Substring(i, 2);
                        MESAJ_CHC.MESAJ_MESAJ += (char)Convert.ToInt32(buf, 16);

                    }

                    MESAJ_CHC.MESAJ_CRC = CHC_CRC_HESAPLA(buffer_mesaj);
                    return 1;

                }
                else
                {
                    return 0;
                }
            }
            catch
            {
                return 0;
            }
        }

        public static string CHC_OLUSTURUCU(string chc_mesaj)
        {
            return "asd";
        }

        public static int CHC_CRC_HESAPLA(string chc_mesaj_mesaj)
        {
            int sayac = 0;

            for (int i = 0; i < chc_mesaj_mesaj.Length; i += 2)
            {

                string output = chc_mesaj_mesaj.Substring(i, 2);
                sayac += Convert.ToInt32(output, 16);

            }

            /*
            for (int i = 0; i < chc_mesaj_mesaj.Length; i++)
            {
                sayac += (int)chc_mesaj_mesaj[i];
            }
            */
            sayac = sayac % 17;
            return sayac;
        }

        public static int CHC_METOD_COZUCU()
        {
            switch (MESAJ_CHC.MESAJ_TYPE)
            {
                case ((int)CHC_METOD_LIST.CIHAZ_BILGI):
                    {
                        CIHAZ_PROFIL_COZUCU(MESAJ_CHC.MESAJ_MESAJ);

                        return 0;
                    }

                default:
                    return 99;

            }
        }

        #region TELETOUCH OZEL METODLAR

        public string CIHAZ_PROFIL_OLUSTURUCU(string cihaz_adi, string yazilim_v, string donanim_v)
        {
            string profil;
            profil = cihaz_adi + "/" + yazilim_v + "/" + donanim_v;
            return profil;
        }

        public static int CIHAZ_PROFIL_COZUCU(string cihaz_profil)
        {
            try
            {
                string[] cihaz_bilgileri = cihaz_profil.Split('/');
                CIHAZ.cihaz_adi = cihaz_bilgileri[0];
                CIHAZ.cihaz_donanim_v = cihaz_bilgileri[1];
                CIHAZ.cihaz_yazilim_v = cihaz_bilgileri[2];
                return 1;

            }
            catch
            {
                return 0;
            }
        }

        #endregion



    }
}
